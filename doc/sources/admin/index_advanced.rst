Advanced features
=================

.. toctree::
   :maxdepth: 1

   smtp
   notifications
   passwordstore
   cda
   rbac
   customfunctions
   extendedfunctions
   resetpassword
   register
   logoutforward
   securetoken
   handlerauthbasic
   ssoaas
   servertoserver
   safejail
   loginhistory
   fastcgi
   fastcgiserver
   psgi
   managertests
   rules_examples
   parameterlist
