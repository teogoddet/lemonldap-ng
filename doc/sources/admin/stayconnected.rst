Stay connected plugin
=====================

This plugin enables persistent connection. It allows us to connect
automatically from the same browser.

Configuration
-------------

Just enable it in the manager (section “plugins”).

-  **Parameters**:

   -  **Activation**: Enable / Disable this plugin
   -  **Expiration time**: Persistent session connection and cookie timeout
   -  **Cookie name**: Persistent connection cookie name